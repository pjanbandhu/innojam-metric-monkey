import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import Header from './components/header';
import Footer from './components/footer';

import Home from './pages/home'
import Insights from './pages/insights'
import Influencers from './pages/influencers'
import Demographics from './pages/demographics'
import Posts from './pages/posts'

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

function App() {
  return (
    <div className="App">
      <Router>
        <Header />
         <Switch>
          <Route path="/insights">
            <Insights />
          </Route>
          <Route path="/posts">
            <Posts />
          </Route>
          <Route path="/demographics">
            <Demographics />
          </Route>
          <Route path="/influencers">
            <Influencers />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
        <Footer />
      </Router>
    </div>
  );
}

export default App;
