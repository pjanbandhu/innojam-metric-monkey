import React from 'react'

import TopInfluencers from '../../components/top-influencers';


const Influencers = props => {
  return (
    <main>
      <div className="container">
        <div className="row">
          <div className="col">
            <h2>Influencers</h2>
            <p>Out of the 50 influencers you handpicked, following might be the <b>TOP FIVE</b> best equipped to meet your marketing goals:</p>
            <div className="container">
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <TopInfluencers />
          </div>
        </div>
      </div>
    </main>
  )
}
export default Influencers