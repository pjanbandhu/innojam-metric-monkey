import React from 'react'
import InstagramDemographics from '../../components/instagram-demographics'


const Demographics = props => {  
  return (
    <main>
      <div className="container">
        <div className="row">
          <div className="col">
            <h2>Demographics</h2>
            <p>GET TO KNOW YOUR FANS</p>
            <p>Who are the people you're connecting with and where are they from?</p>
          </div>
        </div>
      </div>

      <div className="container">
        <div className="row">
          <div className="col">
            <InstagramDemographics />
          </div>
        </div>
      </div>
    </main>
  )
}
export default Demographics