import React from 'react'

import InstagramPosts from '../../components/instagram-posts';

const Posts = props => {
  return (
    <main>
      <div className="container">
        <div className="row">
          <div className="col">
          <h2>Sentiment Analysis</h2>
          <p>Did you recently run a campaign for your brand or host a pop-up store? Are you looking to expand your products and services? What to know what people are saying but don't know where to start? Sentiment analysis is one of the most crucial tools to understand what the world thinks about your brand, business or niche. With our sentiment analysis powered with NLP (Natural language processing), we find out the sentiment behind comments (potential) customers are making on social media today. Track what is working and what isn't. The themes that develop can help shape your business decisions!</p>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col">
          <InstagramPosts/>
        </div>
      </div>
    </main>
  )
}
export default Posts