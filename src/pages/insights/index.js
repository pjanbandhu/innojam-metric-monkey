import React from 'react'

import InstagramTraffic from '../../components/instagram-traffic';
import InstagramReach from '../../components/instagram-reach';

import './insights.scss'

/***
 * To determine the top five locations your brand impacts, 
 * we can determine an approximate location of users that 
 * like our use-case customer's brand, or that show up under
 * a hashtag specific to our customer's niche (not too broad
 * or generic).
 * 
 * Instagram provides us with a location id which corresponds
 * to a lat-lng value. We determine the top 5 most common
 * location IDs that show up from our API results. These would
 * possibly be the top 5 locations our customer can maybe, host
 * their next pop-up store, or organize an event.
 */

const Insights = props => {  
  return (
    <main>
      <div className="container">
        <div className="row">
          <div className="col">
            <h2>Performance</h2>
            <p>INSTAGRAM PERFORMANCE METRICS AND TRENDS FOR YOUR ACCOUNT</p>
            <p>These analytics provide you with your geographical impact, your reach and engagement trend over the year and other impactful data to help you monitor and analyze your interaction as a business, with the world through social media. You can also look this data up over a specific period of time.</p>
          </div>
        </div>
        <div className="statistics-container">
        <div className="row">
          <div className="col">
            <b><p>TOP FIVE LOCATIONS YOUR BRAND IMPACTS</p></b>
            <p>The top locations your brand receives most interaction from.</p>
            <br />
            <InstagramTraffic />
          </div>
        </div>
        <div className="row">
          <div className="col">
            <b><p>REACH</p></b>
            <p>The number of unique accounts that visited your profile over the year. <b>MAY</b> shows your least reach and <b>AUGUST</b> shows the highest reach.</p>
            <InstagramReach height={200} />
          </div>
        </div>
      </div>


      </div>
    </main>
  )
}
export default Insights