import React, { useEffect, useState } from 'react'
import { Link } from "react-router-dom";

import hero from '../../images/heroImage.svg'
import performance from '../../images/performance.svg'
import demographics from '../../images/demographics.svg'
import influencers from '../../images/influencers.svg'
import posts from '../../images/posts.svg'
import search from '../../images/search-24px.svg'

import './home.scss';

const Home = props => {  
  const [firstName, setFirstName] = useState('');

  useEffect(() => {
    fetch('http://localhost:3000/me')
      .then(response => response.json())
      .then(data => {
        /* 
         * In the interest of time and preventing unexpected complications,
         * our team decided to skip the actual instagram integration
         * and mock the responses instead.
         */
        console.log('Result from our mock server that returns a JSON structure exactly like the Instagram Graph API call: ', data)
        setFirstName(data.first_name)
      })
  })

  return (
    <main className="home-page">
      <div className="container-fluid hero">
        <div className="row">
          <div className="col">
            <img src={hero} alt="Metric Monkey Hero" />
          </div>
        </div>
      </div>

      <div className="container">
        <div className="row">
          <div className="col">
            <br/>
            <h1>Welcome, {firstName}!</h1>
            <p>HERE'S AN OVERVIEW OF INSIGHTS THAT MATTER TO YOU:</p>
          </div>
        </div>
      </div>
      <div className="container">
        <div className="row">

          <div className="col-md-6 col-lg-3 home-card">
            <div className="card">
              <div className="card-img-bg pf">
                <img src={performance} className="card-img-top" alt="Performance" /> 
              </div>
              <div className="card-body">
                <h5 className="card-title">Performance</h5>
                <p className="card-text">Gauge and analyze your account's performance and what's working for your brand and what's not.</p>
                <Link to="/insights" className="btn btn-primary">Performance</Link>
              </div>
            </div>
          </div>

          <div className="col-md-6 col-lg-3 home-card">
            <div className="card">
              <div className="card-img-bg d">
                <img src={demographics} className="card-img-top" alt="Demographics" /> 
              </div>
              <div className="card-body">
                <h5 className="card-title">Demographics</h5>
                <p className="card-text">Looking to grow? A bird's eye view of where your impact as a business lies, geographically.</p>
                <Link to="/demographics" className="btn btn-primary">Demographics</Link>
              </div>
            </div>
          </div>
          
          <div className="col-md-6 col-lg-3 home-card">
            <div className="card">
              <div className="card-img-bg i">
                <img src={influencers} className="card-img-top" alt="Influencers" /> 
              </div>
              <div className="card-body">
                <h5 className="card-title">Influencers</h5>
                <p className="card-text">Never too late (or early) to get in the game! Get a headstart into influencer marketing for your brand.</p>
                <Link to="/influencers" className="btn btn-primary">Influencers</Link>
              </div>
            </div>
          </div>
          
          <div className="col-md-6 col-lg-3 home-card">
            <div className="card">
              <div className="card-img-bg po">
                <img src={posts} className="card-img-top" alt="Posts" /> 
              </div>
              <div className="card-body">
                <h5 className="card-title">Sentiment Analysis</h5>
                <p className="card-text">What are people talking about you? Or about your competitors? Or just about cupcakes? Find out.</p>
                <Link to="/posts" className="btn btn-primary">Sentiment Analysis</Link>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="container">
        <div className="row">
          <div className="col">
              <h3>Or, we can find exactly what you're looking for!</h3>
              <p>SEARCH A HASHTAG, USERNAME, TOPIC, ETC...:</p>
            </div>
          </div>
          <div className="col search-all">
            <form>
              <input type="text" class="form-control" id="search-input" aria-describedby="emailHelp" placeholder="Search Instagram" />
              <Link to="/insights" className="btn btn-link"><img src={search} alt="Search" /></Link>
            </form>
          </div>
      </div>
    </main>
  )
}
export default Home