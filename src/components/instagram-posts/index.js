import React, { Component } from 'react'
import { Bar } from 'react-chartjs-2'
import _ from 'lodash';
import { Tabs, Tab } from 'react-bootstrap';

import './instagram-posts.scss'

const moment = require('moment');

class InstagramPosts extends Component {
    constructor(){
        super();
        this.state = { posts: [], chartData: null };
    }
    componentDidMount() {
        fetch('http://localhost:3001/api/analyze-data',
            {
                method: "POST",
                body: [] // body is empty because its getting static data from generate.js
            })
            .then(res => {
                return res.json()
            })
            .then(postsData => {
                this.setState({ postsData });
                this.chartSetup();
            });
    }

    getChartTimestampFormat(timestamp){
        return moment(timestamp).format("l");
    }

    getTabContent(postsData) {
        return  postsData.map(postData => (
            <div key={postData.comment.id}>
                {postData.comment.text}<br />
                <div style={{ fontSize: '10px' }}>{new Date(postData.comment.timestamp).toLocaleDateString()}</div>
            </div>
        ));
    }

    getCommentsBySentiment(sentiment) {
        const { postsData } = this.state;

        switch (sentiment) {
            case "positive": {
                return _.filter(postsData, postData => postData.sentiment >= 0);
            }
            case "negative": {
                return _.filter(postsData, postData => postData.sentiment < 0);
            }
            default: {
                return postsData;
            }
        }
    }

    getTabsCommentsBySentiment(sentiment){
        return this.getTabContent(this.getCommentsBySentiment(sentiment));
    }

    chartSetup(){
        const { postsData } = this.state;
        const xAxisLabels = _.uniq(_.map(postsData, postData => this.getChartTimestampFormat(postData.comment.timestamp))).sort();
        const transformedData = _.reduce(xAxisLabels, (result, lbl) => {
            const postsByDate = _.filter(postsData, postData => this.getChartTimestampFormat(postData.comment.timestamp) === lbl);
            result[lbl] = _.extend({}, {
                positive: _.filter(postsByDate, postData => postData.sentiment >= 0).length,
                negative: _.filter(postsByDate, postData => postData.sentiment < 0).length
            });
            return result;
        }, {});

        const chartData = {
            labels: xAxisLabels,
            datasets: [{
                type: 'bar',
                label: 'Negative',
                backgroundColor: "#c53612",
                barPercentage: 0.5,
                borderColor: 'white',
                borderWidth: 2,
                data: _.map(xAxisLabels, lbl => {
                    return this.commentsCountByDate('negative', lbl, transformedData);
                })
            }, {
                type: 'bar',
                label: 'Positive',
                backgroundColor: "#8BC55C",
                barPercentage: 0.5,
                data: _.map(xAxisLabels, lbl => {
                    return this.commentsCountByDate('positive', lbl, transformedData);
                }),
                borderColor: 'white',
                borderWidth: 2
            }]
        };
        this.setState({ chartData})
    }

    commentsCountByDate(label, date, transformedData) {
        if (label === 'positive') {
            return transformedData[date].positive;
        }
        else {
            return transformedData[date].negative;
        }
    }

    render() {
        if (!this.state.postsData) return <div>Fetching data...</div>;

        const positiveVibesTitle = `Positive vibes (${this.getCommentsBySentiment('positive').length})`;
        const negativeVibesTitle = `Negative vibes (${this.getCommentsBySentiment('negative').length})`;

        return (<div className={'container'}>
            <div>
                {
                    <Tabs defaultActiveKey="positiveVibes" id="uncontrolled-tab-example">
                        <Tab eventKey="positiveVibes" title={positiveVibesTitle} >
                            <br />
                            <div>{this.getTabsCommentsBySentiment('positive')}</div>
                        </Tab>
                        <Tab eventKey="negativeVibes" title={negativeVibesTitle} >
                            <br />
                            <div>{this.getTabsCommentsBySentiment('negative')}</div>
                        </Tab>
                    </Tabs>

                }
            </div>
            <br /><br />
            <div style={{ display: 'flex', justifyContent: 'center'}}>
                {this.state.chartData === null ? <div>Generating chart data...</div> :
                    <Bar data={this.state.chartData} height={500} width={700} />}
            </div>
        </div>);
    }
}

export default InstagramPosts;