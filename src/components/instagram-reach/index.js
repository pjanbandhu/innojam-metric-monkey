import React, { Component } from 'react'
import { Bar } from 'react-chartjs-2'

import './instagram-reach.scss'

/* const data= {
  labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "Ocotber", "November"],
  datasets: [{
  label: "INSTAGRAM MONTHLY REACH TRENDS FOR YOUR ACCOUNT",
  barPercentage: 0.5,
  backgroundColor: 'rgb(255, 99, 132)',
  borderColor: 'rgb(255, 99, 132)',
  data: [0, 10, 5, 2, 20, 30, 45, 60, 54, 49, 43],
  }]
} */

class InstagramReach extends Component {	
  constructor(props) {
    super(props)
    this.state = {
      data: {
        labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "Ocotber", "November"],
        datasets: [{
        label: "INSTAGRAM MONTHLY REACH TRENDS FOR YOUR ACCOUNT",
        barPercentage: 0.9,
        backgroundColor: '#4D94AB',
        borderColor: '#4D94AB',
        data: [3204, 2349, 5234, 4024, 2091, 3023, 4523, 6065, 5456, 4967, 4334], // this data will be received from Instagram's insights API, can be sorted to determine the max and min reach
        }]
      }
    }
  
    fetch('http://localhost:3000/me')
      .then(response => response.json())
      .then(data => {
        const tempData = this.state.data
        tempData.data = data
        console.log(data)
        this.setState({
          data: tempData
        })
      })
  }
  
  render() {
    return (<Bar data={this.state.data} height={this.props.height} width={this.props.width} />)
  }
}

export default InstagramReach