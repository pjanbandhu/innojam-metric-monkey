import React, { Component } from 'react'
import { Pie } from 'react-chartjs-2'

import './instagram-traffic.scss'

const data= {
  labels: ['Somerville, MA','Quincy, MA','Philadelphia, PA','Burlington, MA','Pittsburgh, PA'],
  datasets: [{
    backgroundColor: ['#4D94AB','#EDAE49','#FE5F55','#D1495B','#09BC8A'],
    data: [10, 20, 30, 35, 5],
  }],
}

const options = {
  tooltips: {
    bodyFontSize: 14
  },
  responsive: false,
  maintainAspectRatio: false,
  cutoutPercentage: 50,
  legend:{
    display:true,
    position:'bottom'
  }
}

class InstagramTraffic extends Component {	
  render() {
    return (<div className="chart-container instagram-traffic"><Pie data={data} options={options} width={350} height={350} /></div>)
  }
}

export default InstagramTraffic