import React from 'react'

import './footer.scss';

const Footer = props => {  
  const today = new Date();
  return (
    <footer>
      <div className="container-fluid">
        <div className="row">
          <div className="col">&copy; {today.getFullYear()} Metric Monkey InnoJam</div>
        </div>
      </div>
    </footer>
  )
}
export default Footer