import React, { Component } from 'react'
import { HorizontalBar } from 'react-chartjs-2'
import { InputGroup, FormControl, Button } from 'react-bootstrap';

import './top-influencers.scss'
import Influencers from '../../pages/influencers'

let dataReach= {
  labels: ['@sukicat','@missenell','@smoothiethecat','@merlinragdoll','@simonscatofficial'],
  datasets: [{
    backgroundColor: ['#4D94AB','#EDAE49','#FE5F55','#D1495B','#09BC8A'],
    data: [5012, 4398, 2817, 2030, 2523],
  }],
}

const dataImpressions= {
  labels: ['@sukicat','@missenell','@smoothiethecat','@merlinragdoll','@simonscatofficial'],
  datasets: [{
    backgroundColor: ['#4D94AB','#EDAE49','#FE5F55','#D1495B','#09BC8A'],
    data: [42344, 75223, 23982, 48359, 57468],
  }],
}

const dataEngagementRate= {
  labels: ['@sukicat','@missenell','@smoothiethecat','@merlinragdoll','@simonscatofficial'],
  datasets: [{
    backgroundColor: ['#4D94AB','#EDAE49','#FE5F55','#D1495B','#09BC8A'],
    data: [4.7, 5.6, 3.6, 3.2, 4.5],
  }],
}

const dataOverall= {
  labels: ['@sukicat','@missenell','@smoothiethecat','@merlinragdoll','@simonscatofficial'],
  datasets: [{
    backgroundColor: ['#4D94AB','#EDAE49','#FE5F55','#D1495B','#09BC8A'],
    data: [4, 5, 3, 1, 2],
  }],
}

const options = {
  tooltips: {
    bodyFontSize: 14
  },
  responsive: false,
  maintainAspectRatio: false,
  cutoutPercentage: 50,
  legend:{
    display:false,
    position:'bottom'
  }
}

class TopInfluencers extends Component {
  constructor(props) {
    super(props)

    this.state = {
      dataReachTemp: {
        labels: ['@sukicat','@missenell','@smoothiethecat','@merlinragdoll','@simonscatofficial'],
        datasets: [{
          backgroundColor: ['#4D94AB','#EDAE49','#FE5F55','#D1495B','#09BC8A'],
          data: [] // [5012, 4398, 2817, 2030, 2523]
        }]
      }
    }

    const influencers = [ 'sukicat', 'missenell' , 'smoothiethecat', 'merlinragdoll', 'simonscatofficial' ]
    const reach = []

    // Fetching mock data similar to instagram's graph API from our mock server...

    for(var i=0;i<influencers.length;i++) {
      fetch('http://localhost:8000/reach_' + influencers[i])
      .then(response => response.json())
      .then(data => {
        reach.push(data.values[0].value)
      })
      .then(() => {
        this.setState(prevState => {
          let dataReachTemp = Object.assign({}, prevState.dataReachTemp)
          dataReachTemp.datasets[0].data.push(reach[reach.length-1])
          return { dataReachTemp }
        })
      })
    }
  }

  render() {
    console.log(this.state.dataReachTemp, dataReach)
    return (
      <div className="top-influencers">
        <InputGroup>
          <InputGroup.Prepend>
            <InputGroup.Text>Enter Instagram handles (comma separated):</InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl as="textarea" aria-label="With textarea" />
          <Button className="influencer-analyze-button" type="submit">Analyze</Button>
        </InputGroup>
      <div className="chart-container">
        <div>
          <h4>BY REACH</h4>
          <p>The average number of unique account that saw this influencer's posts</p>
          <HorizontalBar data={dataReach} options={options} width={450} height={350} />
        </div>
        <div>
          <h4>BY ENGAGEMENT RATE</h4>
          <p>Engagement rate is calculated using the formular: (Likes + Comments) / Followers * 100 </p>
          <HorizontalBar data={dataEngagementRate} options={options} width={450} height={350} />
        </div>
        <div>
          <h4>BY IMPRESSIONS</h4>
          <p>The average number of times this influencer's posts were seen</p>
          <HorizontalBar data={dataImpressions} options={options} width={450} height={350} />
        </div>
        <div>
          <h4>OVERALL</h4>
          <p>An all-round ranking of these top 5 influencers</p>
          <HorizontalBar data={dataOverall} options={options} width={450} height={350} />
        </div>
      </div>
      </div>)
  }
}

export default TopInfluencers