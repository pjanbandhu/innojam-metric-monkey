import React, { Component } from 'react'
import { Doughnut } from 'react-chartjs-2'

import './instagram-demographics.scss'
import MapChart from '../map/map-chart'

const dataGender = {
  labels: ['Male', 'Female', 'Other'],
  datasets: [{
    data: [44, 53, 3],
    backgroundColor: ['#4D94AB', '#EDAE49', '#FE5F55']
  }]
}
const dataAge = {
  labels: ['18-25', '25-34', '34+'],
  datasets: [{
    data: [58, 31, 11],
    backgroundColor: ['#4D94AB', '#EDAE49', '#FE5F55']
  }]
}
const options = {
  tooltips: {
    bodyFontSize: 14
  },
  responsive: false,
  maintainAspectRatio: false,
  cutoutPercentage: 50,
  legend: {
    display: true,
    position: 'bottom'
  }
}
class InstagramDemographics extends Component {
  render() {
    return (
      <div className="demographics-charts-container">
        <div className="gender">
          <h3>Gender</h3>
          <Doughnut data={dataGender} height={220} width={300} options={options} />
        </div>
        <div className="age">
          <h3>Age group</h3>
          <Doughnut data={dataAge} height={220} width={300} options={options} />
        </div>
        <div className="loc">
          <h3>Top User Locations</h3>
          <div>
            <MapChart />
          </div>
        </div>
      </div>)
  }
}

export default InstagramDemographics