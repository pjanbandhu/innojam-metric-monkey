import React from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import { Link } from "react-router-dom";

import logo from '../../images/logo.svg'

import './header.scss';

const Header = props => {  
  return (
    <Navbar bg="light" expand="lg">
    <Link to="/" className="navbar-brand"><img src={logo} alt="Metric Monkey Logo" /></Link>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="mr-auto">
        <Link to="/insights" className="nav-link">Performance</Link>
        <Link to="/demographics" className="nav-link">Demographics</Link>
        <Link to="/influencers" className="nav-link">Influencers</Link>
        <Link to="/posts" className="nav-link">Sentiment Analysis</Link>
      </Nav>
    </Navbar.Collapse>
    </Navbar>
  )
}
export default Header