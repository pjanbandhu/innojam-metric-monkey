const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const natural = require('natural');
const generate = require('./api/generate');

const API_PORT = 3001;
const app = express();
app.use(cors());
const router = express.Router();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

router.post('/analyze-data', (req, res) => {
    // TODO: had to get the data here instead because relative imports outside src/ are not supported!
    const comments = generate().comments.data;
    const Analyzer = natural.SentimentAnalyzer;
    const stemmer = natural.PorterStemmer;
    const analyzer = new Analyzer("English", stemmer, "afinn");
    const tokenizer = new natural.WordTokenizer();

    const commentsWithSentimentScore = comments.reduce((result, value, index) => {
        result[index] = { comment:  value, sentiment: analyzer.getSentiment(tokenizer.tokenize(value.text)) };
        return result;
    }, []);

    res.status(200).send(commentsWithSentimentScore);
});

app.use('/api', router);

app.listen(API_PORT, () => console.log(`LISTENING ON PORT ${API_PORT}`));
