module.exports = (req, res, next) => {
    var faker = require("faker");
    var _ = require("lodash");
    return {
        me: {
            first_name: "Manny",
            last_name: "Del Gato",
            id: "111127187017942",
          },

        names: _.times(10, function (n) {
            return {
                id: n,
                name: faker.name.findName(),
                avatar: faker.internet.avatar()
            }
        }),

        reach_lifetime: {
            name: "reach",
            period: "lifetime",
            values: [
              {
                value: 103
              }
            ],
            title: "Reach",
            description: "Total number of unique accounts that have seen the media object",
            id: "17855590849148465/insights/reach/lifetime"
          },
        reach_sukicat: {
            name: "reach",
              period: "lifetime",
              values: [
                {
                  value: 5012
                }
              ],
              title: "Reach",
              description: "Total number of unique accounts that have seen the media object",
              id: "17855590849148467/insights/reach/lifetime"
          },
        reach_missenell: {
            name: "reach",
              period: "lifetime",
              values: [
                {
                  value: 4398
                }
              ],
              title: "Reach",
              description: "Total number of unique accounts that have seen the media object",
              id: "17855590849148468/insights/reach/lifetime"
          },
        reach_smoothiethecat: {
            name: "reach",
              period: "lifetime",
              values: [
                {
                  value: 2817
                }
              ],
              title: "Reach",
              description: "Total number of unique accounts that have seen the media object",
              id: "17855590849148469/insights/reach/lifetime"
          },
        reach_merlinragdoll: {
            name: "reach",
              period: "lifetime",
              values: [
                {
                  value: 2030
                }
              ],
              title: "Reach",
              description: "Total number of unique accounts that have seen the media object",
              id: "17855590849148469/insights/reach/lifetime"
          },
        reach_simonscatofficial: {
            name: "reach",
              period: "lifetime",
              values: [
                {
                  value: 2523
                }
              ],
              title: "Reach",
              description: "Total number of unique accounts that have seen the media object",
              id: "17855590849148470/insights/reach/lifetime"
          },

        comments: {
            "data": [
                {
                    "timestamp": "2019-11-19T18:10:30+0000",
                    "text": "I love this!",
                    "id": "17873440459141021"
                },
                {
                    "timestamp": "2019-11-19T19:16:02+0000",
                    "text": "This is awesome!",
                    "id": "17870913679156914"
                },
                {
                    "timestamp": "2019-11-20T18:10:30+0000",
                    "text": "Ugh, i hate rainy, cloudy weather!",
                    "id": "17873440459141023"
                },
                {
                    "timestamp": "2019-11-20T19:16:02+0000",
                    "text": "its great!, thanks for sharing",
                    "id": "17870913679156915"
                },
                {
                    "timestamp": "2019-11-20T19:16:02+0000",
                    "text": "I heart hackathon",
                    "id": "17870913679156916"
                },

                {
                    "timestamp": "2019-11-21T18:10:30+0000",
                    "text": "I dont know why you do this!",
                    "id": "17873440459141022"
                },
                {
                    "timestamp": "2019-11-21T19:16:02+0000",
                    "text": "Super!",
                    "id": "17870913679156913"
                },
                {
                    "timestamp": "2019-11-21T18:10:30+0000",
                    "text": "wth, seriously?",
                    "id": "17873440459141024"
                },
                {
                    "timestamp": "2019-11-21T19:16:02+0000",
                    "text": "I am surprised!",
                    "id": "17870913679156917"
                },
                {
                    "timestamp": "2019-11-21T19:16:02+0000",
                    "text": "This usually doesn't happen in day to day life. Weird!",
                    "id": "17870913679156918"
                },
                {
                    "timestamp": "2019-11-22T19:16:02+0000",
                    "text": "A good one ✌",
                    "id": "27870913679156917"
                },
                {
                    "timestamp": "2019-11-22T19:16:02+0000",
                    "text": "Nice! ✌",
                    "id": "27870913679156918"
                },

                {
                    "timestamp": "2019-11-22T19:16:02+0000",
                    "text": "Pretty picture",
                    "id": "37870913679156917"
                },
                {
                    "timestamp": "2019-11-22T19:16:02+0000",
                    "text": "Great business idea",
                    "id": "47870913679156918"
                },
                {
                    "timestamp": "2019-11-22T19:16:02+0000",
                    "text": "Ahahahaha 😆",
                    "id": "57870913679156918"
                },
            ]
        },
    }
}